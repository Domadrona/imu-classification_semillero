# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 15:10:10 2020

@author: Eider Pereira
"""
#%% PAQUETES
import numpy as np
# import funciones as fn
import math as mt
import os
#%% FUNCIONES
def read_data(name):
    file = open(name)
    file = file.readlines()
    data = np.zeros((3,))
    for i in file:
        j = i.split(sep=',')
        try:
            j = np.asarray(j[:3],dtype='float64')
            data = np.vstack((data,j))
        except:
            continue
    data = np.delete(data, 0, axis = 0)
    return data

"""
La función 'RMS' está configurada para trabajar sin solape y para un sólo eje
o canal, entregando el valor de RMS con longitud del número de particiones
establecidas
Parámetros:
    -Signal: Un sólo canal o un sólo eje de la señal (eje X,Y o Z en este caso)
    -num_windows: Establece el número de particiones que se le hará a la señal
 
"""
def RMS(signal, num_windows):
    RMS = []
    window = mt.floor(len(signal)/num_windows)
    for start in range(0,len(signal),window):
        end = start + (window-1)
        if end >= len(signal):
            end = len(signal)-1
        rms = np.sqrt(np.mean(np.power(signal[start:end],2)))
        RMS.append(rms)
    if len(RMS) > num_windows:
        RMS.pop(-1)
    return np.asarray(RMS)
"""
La función 'RMS_axis' se encarga de entregar los valores de la función
'RMS' en todos los ejes
"""
def RMS_axis(signal, num_windows):
    RMS_all_axis = []
    for i in range(signal.shape[1]):            
        RMS_one_axis = RMS(signal[:,i],num_windows)
        RMS_all_axis.extend(RMS_one_axis)
    return np.asarray(RMS_all_axis)

def SSI(signal):
    ssi = np.sum(np.power(signal,2))
    return ssi

def SSI_axis(signal):
    ssi_axis = []
    for i in range(signal.shape[1]):
        ssi_axis.append(SSI(signal[:,i]))
    return np.asarray(ssi_axis)

def MEAN(signal):
    mean = np.mean(signal)
    return mean

def MEAN_axis(signal):
    mean_axis = []
    for i in range(signal.shape[1]):
        mean_axis.append(MEAN(signal[:,i]))
    return np.asarray(mean_axis)
# ____________FRECUENCIA_____________________
def FFT(signal,Fs):
    P = np.fft.fft(signal)
    P2 = np.abs(P[0:int(len(P)/2)])
    F = np.linspace(0,Fs/2,int(len(P)/2))
    return F, P2

def FFT_axis(signal,Fs):
    Pot  = []
    for i in range(signal.shape[1]):
        Fr,Pt = FFT(signal[:,i],Fs)
        Pot.append(Pt)
    return Fr, np.asarray(Pot).T

def Pot_mean(P):
    return np.mean(P)

def Pot_mean_axis(P):
    Pot = []
    for i in range(P.shape[1]):
        Pot.append(Pot_mean(P[:,i]))
    return np.asarray(Pot)

def f_mean(P,F):
    return np.sum(P*F)/np.sum(P)

def f_mean_axis(P,F):
    F_mean = []
    for i in range(P.shape[1]):
        F_mean.append(f_mean(P[:,i],F))
    return np.asarray(F_mean)

#guardar max min de normalizar vector de nueva señal
#leeer(si myor a 2600 puntos), carac, normalizar, predict
def normalize_colum(data):
    X = np.zeros((data.shape[0],data.shape[1]))
    for i in range(data.shape[1]):
        X[:,i] = (data[:,i]-data[:,i].min())/(data[:,i].max()-data[:,i].min())
    return X
#%% IMPLEMENTACIÓN
##dirección / not \
path = os.listdir('D:/Documents/ITM 2020-2/semillero/signals')
car_all_axis = []
Fs = 51
num_ven_RMS = 5
y = []
for S in path:
    y.append(int(S[4]))
    sACC = read_data(S)
    # Carecterísticas Tiempo
    SSI_data = SSI_axis(sACC)
    RMS_data = RMS_axis(sACC, num_ven_RMS)
    mean = MEAN_axis(sACC)
    # Características Frecuencia
    Freq,Pot = FFT_axis(sACC,Fs)
    Pot_Mean = Pot_mean_axis(Pot)
    Freq_mean = f_mean_axis(Pot,Freq)
    #Construcción de la matriz de características
    car_all_axis.append(list(SSI_data))
    #car_all_axis[-1].extend(mean)
    car_all_axis[-1].extend(RMS_data)
    car_all_axis[-1].extend(Pot_Mean)
    car_all_axis[-1].extend(Freq_mean)
car_all_axis = np.asarray(car_all_axis)
X = normalize_colum(car_all_axis)
y = np.asarray(y)

#%%
"SVM"
from sklearn.svm import SVC
import time
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
# X=features_mat[:,0:-1]
# x=np.array(xl)
# yl=features_mat[:,-1]
# y=np.array(yl)

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3)

tic = time.time()
clf = SVC(kernel='linear')
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
svm=classification_report(y_test, y_pred)
print(classification_report(y_test, y_pred))
print('Duración svm: ',time.time() - tic)
print("Accuracy:",round(metrics.accuracy_score(y_test, y_pred)*100,2))
print("Train",clf.score(X_train, y_train))
print(confusion_matrix(y_test, y_pred))#, labels=["M1", "M2", "M3", "M4", "M5"]


plot_confusion_matrix(clf, X_test, y_test)  # doctest: +SKIP
plt.show()  # doctest: +SKIP

#%%
"grid search"
from sklearn.model_selection import GridSearchCV
# xl=features_mat[:,0:-1]
# x=np.array(xl)
# yl=features_mat[:,-1]
# y=np.array(yl)
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3)#,random_state=42 para dar continuidad
# scale=preprocessing.MinMaxScaler()

tic = time.time()
svc=SVC()#SVC(kernel='linear')
# pipe_svc=Pipeline([('scaler',scale),('svc',svc)])
# acc=cross_val_score(pipe_svc,X_train,y_train,cv=10)
# print(acc.mean())
#,'rbf' tumba resultados de grid search 0.95 a 0.9
params = {
        "gamma": np.logspace(-3,3,7),
        'kernel':('linear','sigmoid','rbf'), 
        'C':np.logspace(-3,5,9),}

search = GridSearchCV(svc, params,scoring = 'accuracy', cv=3)
search.fit(X_train, y_train)
grid_predictions = search.predict(X_test)  
duracion = time.time() - tic
print('Duración : ', duracion)

print("Best parameter :" , search.best_params_)#media del mejor estimador según CV CONSULTAR CUAL METRICA
print("score train = %0.2f" % search.best_score_)
# target_names = ["M1","M2","M3","M4","M5"]
print(classification_report(y_test, grid_predictions))#, target_names = target_names)
d = classification_report(y_test, grid_predictions, output_dict=True)#, target_names = target_names
print("score test = %0.2f" % d['accuracy'])

cvr=search.cv_results_
std=cvr['std_test_score'][search.best_index_]

svm = [round(search.best_score_*100), round(d['accuracy']*100), round(std*100,2), round(duracion,4), search.best_params_]
print("Train Test   SD time Params")
print(round(search.best_score_*100),"   ", round(d['accuracy']*100)," ", round(std*100,2), round(duracion,2), search.best_params_)
